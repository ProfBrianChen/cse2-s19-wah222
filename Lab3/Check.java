import java.util.*;
public class Check{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in);     //GATHERING INPUT DATA
    System.out.println("Enter the original cost in the form XX:XX: ");
    double checkCost = myScanner.nextDouble();
    System.out.println("Enter the percentage tip you wish to pay as a whole number in the form XX: ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    System.out.println("Enter the number of people who went out to eat: ");
    int ppl = myScanner.nextInt();
    
    double totalCost;             //CALCULATING NUMBER OF DOLLARS, DIMES AND PENNIES PER PERSON
    double costPerPerson;
    int dollars,
      dimes, pennies; 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / ppl;
    dollars = (int)costPerPerson;
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //PRINTING COST PER PERSON
  }
}