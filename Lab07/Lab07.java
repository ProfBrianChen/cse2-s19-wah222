public class Lab07{
  public static void main(String[] args) {
    paragraph(); //CALLS METHOD THAT MAKES PARAGRAPHS
  }
  public static void paragraph(){
    String object = thesis(); //COLLECTS THE NOUN FROM THE THESIS METHOD
    int actions = (int)(Math.random() * 8 + 1);
    for(int i = 1; i <= actions; i++){
        action(object); //A RANDOM NUMBER OF ACTION SENTENCES WILL BE MADE
    }
    conclusion(object); //CONCLUSION
  }
  public static String thesis(){ //THE METHODS USE DATA FROM THE DATABASES OF WORD TYPES
    String object = noun();
    System.out.print("The " + adjective() + " " + color() + " " + object + " " + verb() + " the " + adjective() + " " + noun() + ".");
    return object;
  }
  public static void action( String object){
      System.out.print(" This " + object + " was " + acting() + " " + adjective() + " to the " + verbing() + " " + noun() + ".");
      int choice = (int)(Math.random()*2);
      if(choice == 0){
          object = "It"; //RANDOMIZES IT OR THE ACTUAL NOUN
      }
      else{
          object = "The " + object;
      }
      System.out.print(" " + object + " used " + items() + " to the " + verbed() + " " + noun() + "s at the " + adjective() + " " + noun() + ".");
  }
  public static void conclusion( String object){
      System.out.print(" That" + object + " " + trait() + " her " + noun() + ".");
      System.out.println();
  }
  public static String trait(){ //A LOT OF DATABASES
    String word = "";
    int num = (int)(Math.random()*3);
    switch(num) {
      case 0:
        word = "understood";
        break;
      case 1:
        word = "learned";
        break;
      case 2:
        word = "adopted";
        break;
      case 3:
        word = "loved";
        break;
    }
    return word;
  }
  public static String verbed(){
    String word = "";
    int num = (int)(Math.random()*5);
    switch(num) {
      case 0:
        word = "throw";
        break;
      case 1:
        word = "launch";
        break;
      case 2:
        word = "send";
        break;
      case 3:
        word = "catapult";
        break;
      case 4:
        word = "transfer";
        break;
      case 5:
        word = "dish";
        break;
    }
    return word;
  }
  public static String items(){
    String word = "";
    int num = (int)(Math.random()*5);
    switch(num) {
      case 0:
        word = "toy cars";
        break;
      case 1:
        word = "guns";
        break;
      case 2:
        word = "chocolates";
        break;
      case 3:
        word = "fruits";
        break;
      case 4:
        word = "underwear";
        break;
      case 5:
        word = "ice cream";
        break;
    }
    return word;
  }
  public static String acting(){
    String word = "";
    int num = (int)(Math.random()*5);
    switch(num) {
      case 0:
        word = "particularly";
        break;
      case 1:
        word = "specifically";
        break;
      case 2:
        word = "greatly";
        break;
      case 3:
        word = "extremely";
        break;
      case 4:
        word = "gently";
        break;
      case 5:
        word = "undoubtedly";
        break;
    }
    return word;
  }
  public static String verbing(){
    String word = "";
    int num = (int)(Math.random()*9);
    switch(num) {
      case 0:
        word = "running";
        break;
      case 1:
        word = "jumping";
        break;
      case 2:
        word = "smilling";
        break;
      case 3:
        word = "crying";
        break;
      case 4:
        word = "yelling";
        break;
      case 5:
        word = "thinking";
        break;
      case 6:
        word = "failing";
        break;
      case 7:
        word = "growing";
        break;
      case 8:
        word = "rising";
        break;
      case 9:
        word = "dodging";
        break;
    }
    return word;
  }
  public static String adjective(){
    String word = "";
    int num = (int)(Math.random()*9);
    switch(num) {
      case 0:
        word = "small";
        break;
      case 1:
        word = "quick";
        break;
      case 2:
        word = "slow";
        break;
      case 3:
        word = "ugly";
        break;
      case 4:
        word = "fat";
        break;
      case 5:
        word = "smart";
        break;
      case 6:
        word = "pleasant";
        break;
      case 7:
        word = "charming";
        break;
      case 8:
        word = "slow";
        break;
      case 9:
        word = "tall";
        break;
    }
    return word;
  }


  public static String color(){
    String word = "";
    int num = (int)(Math.random()*9);
    switch(num) {
      case 0:
        word = "blue";
        break;
      case 1:
        word = "green";
        break;
      case 2:
        word = "red";
        break;
      case 3:
        word = "orange";
        break;
      case 4:
        word = "yellow";
        break;
      case 5:
        word = "pink";
        break;
      case 6:
        word = "black";
        break;
      case 7:
        word = "white";
        break;
      case 8:
        word = "violet";
        break;
      case 9:
        word = "indigo";
        break;
    }
    return word;
  }


    public static String noun(){
    String word = "";
    int num = (int)(Math.random()*9);
    switch(num) {
      case 0:
        word = "fox";
        break;
      case 1:
        word = "dog";
        break;
      case 2:
        word = "cat";
        break;
      case 3:
        word = "bird";
        break;
      case 4:
        word = "cheetah";
        break;
      case 5:
        word = "mole";
        break;
      case 6:
        word = "snake";
        break;
      case 7:
        word = "elephant";
        break;
      case 8:
        word = "giraffe";
        break;
      case 9:
        word = "zebra";
        break;
    }
    return word;
  }


    public static String verb(){
    String word = "";
    int num = (int)(Math.random()*9);
    switch(num) {
      case 0:
        word = "passed";
        break;
      case 1:
        word = "hit";
        break;
      case 2:
        word = "swallowed";
        break;
      case 3:
        word = "criticized";
        break;
      case 4:
        word = "helped";
        break;
      case 5:
        word = "decapitaded";
        break;
      case 6:
        word = "outran";
        break;
      case 7:
        word = "painted";
        break;
      case 8:
        word = "taught";
        break;
      case 9:
        word = "shot";
        break;
    }
    return word;
  }
}