import java.util.Scanner;
public class Searching{
    public static void main(String args[]){

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 0 for a linear search or 1 for binary search: ");
        int choice = scan.nextInt();

        if( choice == 0){
            System.out.println("Enter the array size: ");
            int size = scan.nextInt();
            System.out.println("Enter the integer search term: ");
            int num = scan.nextInt();
            int[] arr1 = generate( size );
            System.out.print("Array: {");
            for(int i = 0; i < arr1.length; i++){
                System.out.print(arr1[i] + " ");
            }
            System.out.println("}");
            System.out.println("Index: " + linear( arr1, num));
        }
        else if( choice == 1){
            System.out.println("Enter the array size: ");
            int size = scan.nextInt();
            System.out.println("Enter the integer search term: ");
            int num2 = scan.nextInt();
            int[] arr2 = ascend( size );
            System.out.print("Array: {");
            for(int i = 0; i < arr2.length; i++){
                System.out.print(arr2[i] + " ");
            }
            System.out.println("}");
            System.out.println("Index: " + binary( arr2, num2));
        }
    }
    public static int[] generate( int num ){

        int[] boy = new int[num];

        for( int i = 0; i < num; i++){
            int in = (int)(Math.random() *num);
            boy[i] = in;
        }
        return boy;
    }
    public static int[] ascend( int num ){

        int[] boy = new int[num];
        int counter = 0;

        for( int i = 0; i < num; i++){
            int in = (int)(Math.random() *10);
            boy[i] = in + counter;
            counter += in;
        }
        return boy;
    }
    public static int linear( int[] arr, int num ){

        int index = 0;

        while( true ){
            if( index == arr.length ){
                index = -1;
                break;
            }
            else if( arr[index] == num ){
                break;
            }
            index++;
        }
        return index;
    }
    public static int binary( int[] arr, int num ){

        int a = 0;
        int b = arr.length -1;
        int index = 0;
        int counter = 0;
        
        while( true ){
            if( arr[a] == num){
                index = a;
                break;
            }
            else if( arr[b] == num){
                index = b;
                break;
            }
            else if( (arr[b] - arr[a])/2 > num){
                b = (int)((b - a)/2);
            }
            else if( (arr[b] - arr[a])/2 < num){
                a = (int)((b - a)/2);
            }

            if( counter == arr.length -1 && num != arr[counter]){
                index = -1;
                break;
            }
            counter++;
        }
        return index;
    }
}