import java.util.Scanner;
public class Argyle{
    public static void main(String args[]){
        Scanner ears = new Scanner(System.in);

        int sWidth = collect("width of viewing plane"); //COLLECTION OF SPECS USING METHODS
        int sHeight = collect("height of viewing plane");
        int tWidth = collect("width of Argyle diamonds");
        int dWidth = collect3("width of center stripe", tWidth);
        char backFill = collect2("the backfill");
        char diFill = collect2("the diamond fill");
        char argyFill = collect2("the Argyle fill");



        int numY = 0; //THE NUMBER OF ADDITIONAL BOXES PASSED VETICALLY SHOULD NOT BE RESET, SINCE THE LOOPS GO ONLY DOWN AND NOT BACK UP ROWS

        for( int row = 1; row <= sHeight; row++){
            int numX = 0; //THE NUMBER OF ADDITIONAL BOXES PASSED HORIZONTALLY SHOULD BE RESET TO ZERO EVERY NEW ROW SINCE THE LOOP RETURNS ALL THE WAY TO THE LEFT

            if( (row-1) % (tWidth * 2) == 0 && row != 1){ //CHECKS IF A NEW BOX WAS PASSED VERTICALLY AFTER EVERY ROW, TAKING INTO ACCOUNT THE CASE WHERE ROW = 1
                numY++;
            }
            for( int column = 1; column <= sWidth; column++){
                if((column - 1) % (tWidth * 2) == 0 && column != 1){ //CHECKS IF A NEW BOX WAS PASSED HORIZONTALLY AFTER EVERY COLUMN, TAKING INTO ACCOUNT COLUMN ONE'S SPECIAL CASE
                    numX++;
                }

                //ARGYLE THAT GO FROM LEFT TO RIGHT
                if( (row > tWidth * 2 * numY && row <= tWidth * 2 * numY + tWidth * 2 ) && (column >= -1 * (int)(dWidth / 2) + ((row - numY * tWidth * 2) - 1) + tWidth * 2 * numX + 1 && column <= Math.round(dWidth / 2) + ((row - numY * tWidth * 2) - 1) + tWidth * 2 * numX + 1) && (column > tWidth * 2 * numX && column <= tWidth * 2 * numX + tWidth * 2)){
                        System.out.print(argyFill); //ENSURES ROWS ARE WITHIN EACH INTERVAL OF AN 'X' LOOKING FIGURE(TWIDTH*2). FOUND INTERVAL IN FIRST ROW THEN NOTICED THAT EACH BOUND INCREASES BY ONE FOR EVERY NEW ROW. RESETS EVERY NEW INTERVAL OF "X" LOOKING FIGURE.
                }
                //ARGYLE THAT GO FROM RIGHT TO LEFT
                else if( (row > tWidth * 2 * numY && row <= tWidth * 2 * numY + tWidth * 2 ) && (column <= tWidth * 2 * (numX+1) - (-1 * (int)(dWidth / 2) + ((row - numY * tWidth * 2) - 1) + tWidth * 2 * numX) + tWidth * 2 * numX && column >= tWidth * 2 * (numX+1) - (Math.round(dWidth / 2) + ((row - numY * tWidth * 2) - 1) + tWidth * 2 * numX) + tWidth * 2 * numX && (column > tWidth * 2 * numX && column <= tWidth * 2 * numX + tWidth * 2))){
                        System.out.print(argyFill); //SIMILAR BUT NOW I HAD TO SUBTRACT THE FIRST HALF OF ROWS(TWIDTH) AND THEN REVERSE THOSE VALUES FOR THE SECOND HALF(SO I CAN USE THE FOUND VALUES AS THE TOLERABLE DIFFERENCE GIVEN EACH ROW)
                }



                //THE INNER DIAMOND'S TOP HALF
                else if( ( row > tWidth * 2 * numY && row <= tWidth * 2 * numY + tWidth) && (column > tWidth * 2 * numX && column <= tWidth * 2 * numX + tWidth * 2) && (( Math.abs((column - tWidth * 2 * numX) - tWidth -0.5) < row - tWidth * 2 * numY +0.5))){
                        System.out.print(diFill); //ENSURES ROWS ARE IN EVERY FIRST HALF AND DIFFERENCE BETWEEN CURRENT COLUMN AND MIDDLE VALUE(TWIDTH) IS WITHIN THE ROW NUMBER(FROM THE MIDDLE IF SECOND HALF) SINCE USING ROW NUMBER AS RANGE WORKS
                }
                //THE BOTTOM HALF
                else if( ( row > tWidth * 2 * numY + tWidth && row <= tWidth * 2 * numY + tWidth * 2) && (column > tWidth * 2 * numX && column <= tWidth * 2 * numX + tWidth * 2) && ( Math.abs((column - tWidth * 2 * numX) - tWidth -0.5) <= tWidth - (row - tWidth * 2 * numY - tWidth +0.5))){
                        System.out.print(diFill); //ENSURES ROWS ARE IN EVERY SECOND HALF AND DIFFERENCE BETWEEN CURRENT COLUMN AND MIDDLE VALUE(TWIDTH) IS WITHIN THE ROW NUMBER(FROM THE MIDDLE IF SECOND HALF) SINCE USING ROW NUMBER AS RANGE WORKS
                }

                else{
                    System.out.print(backFill); //IF NO SPECIAL CASES ARE MET, MUST BE BACKFILL
                }
            }
            System.out.println(); //PRINTS A LINE SKIP TO BEGIN THE NEXT ROW
        }
        if( dWidth >= 2 * tWidth){
            System.out.println("\nNote: Having the center stripe thicker than the diamonds does not work well.\n");
        }//NOTE FOR CASE WHERE CENTER STRIPE IS MADE TO BE LARGER THAN DIAMOND SIZE(WHICH WONT MAKE SENSE)

    }


    public static int collect( String want ){
        int goodBoy = 0; //GOING TO BE EDITED
        Scanner ear = new Scanner(System.in);
        while( true ){
            System.out.println("Enter a positive integer value for " + want + ": ");  //promts
            if( ear.hasNextInt() ){   //Checks if there is an int
        	    int value = ear.nextInt();  //Assigns this int into a preliminary value length
                if( value > 0 ){ //Check if it is positive
                    goodBoy = value; //Assigns value to integer
                    break; //Leaves loop
                }
                else{
                    System.out.println("That is not a positive integer.");  //Case where negative int is entered
                }
            }
            else{
            System.out.println("Wrong type."); //Case where letter is entered
            ear.next(); //Trash value
            }
        }
        return goodBoy;
    }


    public static char collect2(String need){
        Scanner listen = new Scanner(System.in);
        char output = 'x'; //TO BE CHANGED
        while(true){
            System.out.println("Enter character to serve as " + need + ": "); //'need' IS USED TO IDENTIFY WHAT IS BEING COLLECTED
            if( listen.hasNextDouble()){
                System.out.println("Enter a character, not a number."); //CHECKS IF A NUMBER WAS ENTERED, IF SO, IT IS TRASHED
                listen.nextDouble();
            }
            else{
                String input = listen.next(); //IF NOT A NUMBER, MUST BE STRING
                output = input.charAt(0); //TAKES FIRST CHARACTER
                break;
            }
        }
    return output;   
    }


    public static int collect3( String want, int width ){
        int goodBoy = 0; //WILL BE CHANGED
        Scanner ear = new Scanner(System.in);
        while( true ){
            System.out.println("Enter a positive odd integer value for " + want + ": ");  //promts
            if( ear.hasNextInt() ){   //Checks if there is an int
        	    int value = ear.nextInt();  //Assigns this int into a preliminary value length
                if( value > 0 ){ //Check if it is positive
                    if( value % 2 == 1 || value == 1){ //Checks if odd
                        if(width / 2 >= value){ //Checks if less than half of diamond size
                            goodBoy = value; //Assigns value to integer
                            break; //Leaves loop
                        }
                        else{
                            System.out.println("Thickness cannot be bigger than half of diamond size"); //Case where not allowed is entered
                        }
                    }
                    else{
                        System.out.println("That is not an odd integer"); //Case where even int is entered
                    }
                }
                else{
                    System.out.println("That is not a positive integer.");  //Case where negative int is entered
                }
            }
            else{
            System.out.println("Wrong type."); //Case where letter is entered
            ear.next(); //Trash value
            }
        }
        return goodBoy;
    }
}