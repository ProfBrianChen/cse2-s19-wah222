import java.util.*;
public class Letters{
    public static void main(String args[]){
        String input = "";
        int length = (int) (Math.random() * 10);
        for(int i = 0; i < length; i++){
            int n = (int)(Math.random() * 50); //CREATES A RANDOM STRING ARRAY
            if( n <= 25 ){
                input += (char)(n + 65); //ASSIGNS RANDOM UPPERCASES
            }
            else{
                input += (char)(n + 72); //AND LOWERCASES
            }
        }
        System.out.println("Random Character array = " + input);

        int n = input.length();
        int[] arr = new int[n];
        for(int i = 0; i < n; i++){ //ASSIGNS DECIMAL OF EACH CHAR
            arr[i] = input.charAt(i);
        }
        char[] arr2 = order( arr ); //CALLS METHOD TO ORDER ARRAY
        System.out.println("The characters, in order are: ");
        for(int i = 0; i < n; i++){
            System.out.print( arr2[i] ); //PRINTS ORDERED LETTERS
        }
        char[] arr3 = arr2;
        char[] arr4 = arr2;
        char[] AtoM = getAtoM( arr3 ); //RETREIVES A NEW ARRAY WITH LETTERS A THROUGH M
        char[] NtoZ = getNtoZ( arr4 ); //AND N TO Z

        System.out.println("\nThe ordered A through M are: ");
        for(int t = 0; t < AtoM.length; t++){ //PRINTS RANGE
            System.out.print(AtoM[t]);
        }
        System.out.println("\nThe ordered N through Z are: ");
        for(int d = 0; d < NtoZ.length; d++){ //PRINTS RANGE
            System.out.print(NtoZ[d]);
        }
        System.out.println();

    }
    public static char[] order( int[] arr ){ //METHOD LOOPS THROUGH ARRAY REPEATEDLY AND FINDS MIN EACH TIME TO THEN ORDER
        int absmin = 0;
        int[] mid = arr;
        char[] arr2 = new char[ arr.length];
        for(int i = 0; i < arr.length; i++){
            int min = 123;
            int upper = 0;
            for(int j = 0; j < mid.length; j++){
                if( mid[j] < 97 ){ //UPPERCASE LETTERS
                    if( mid[j] + 32 < min + 32 * upper){
                        min = mid[j];
                        upper = 1;
                        absmin = j;
                    }
                }
                else if( mid[j] < min + 32 * upper){ //LOWERCASE
                    min = mid[j];
                    absmin = j;
                    upper = 0;
                }
            }
            arr2[i] = (char)min;
            mid[absmin] = 124;
        }
        return arr2;
    }
    public static char[] getAtoM( char[] arr){ //USES ORDERED ARRAY TO LOOP THROUGH ARRAY AND FIND A TO M CHARS
        int counter = 0;
        for( int i = 0; i < arr.length; i++){
            if( arr[i] < 97){
                if( arr[i] <= 77){
                    counter++;
                }
            }
            else{
                if( arr[i] <= 109){
                    counter++;
                }
            }
        }
        char[] AtoM = new char[counter];
        for(int i = 0; i < counter; i++){
            AtoM[i] = (char)(arr[i]);
        }
        return AtoM;
    }
    public static char[] getNtoZ( char[] arr ){ //FINDS N-Z
        int counter = 0;
        for( int i = 0; i < arr.length; i++){
            if( arr[i] < 97 ){
                if( arr[i] > 77){
                    counter++;
                }
            }
            else{
                if( arr[i] > 109){
                    counter++;
                }
            }
        }
        char[] NtoZ = new char[counter];
        int index = 0;
        for(int i = arr.length - counter; i < arr.length; i++){
            NtoZ[index] = (char)(arr[i]);
            index++;
        }
        return NtoZ;
    }
}