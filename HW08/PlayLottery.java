import java.util.Scanner;
public class PlayLottery{
    public static void main(String args[]){

        Scanner scan = new Scanner(System.in);

        int[] guesses = new int[5];
        for(int i = 0; i<=4;i++){ //MAKES ARRAY OF GUESSES
            System.out.println("Enter guess for number " + (i+1) + " : ");
            guesses[i] = scan.nextInt();
        }

        int[] winning = numbersPicked(); //MAKES ARRAY OF WINNERS
        System.out.print("\nThe winning numbers are: ");
        for(int i = 0; i <=4;i++){
            System.out.print(winning[i] + " "); //SHOWS WINNERS
        }

        boolean check = userWins( winning, guesses); //CHECKS FOR A WIN
        if( check ){
            System.out.println("\nYou have won the lottery, congrats!");
        }
        else{
            System.out.println("\nYou lose.");
        }

    }
    public static int[] numbersPicked(){

        int[] winning = new int[5];
        for(int n = 0; n <= 4; n++){ //PREASSIGNS DUMMY VALUE TO ARRAY
            winning[n] = 0;
        }
        for(int i = 0; i <=4; i++){
            int random = (int)(Math.random() * 59); //ASSIGNS A RANDOM VALUE WITHIN RANGE AND CHECKS THAT IT IS NOT DUPLICATED
            if(random != winning[0] && random != winning[1] && random != winning[2] && random != winning[3] && random != winning[4]){
                winning[i] = random;
            }
            else{
                i--; //IF SO, GO BACK AND RE DO
            }
        }
        return winning;
    }
    public static boolean userWins( int[] winning, int[] guesses){

        int counter = 0;
        for(int i = 0; i <= 4; i++){
            if( winning[i] == guesses[i]){ //IF BOTH ARRAYS AT INDEX ARE EQUAL, ADD ONE TO COUNTER
                counter++;
            }
        }
        if(counter == 5){ //IF COUNTER IS FIVE, EVERYTHING IS CORRECT
            return true;
        }
        else{
            return false;
        }
    }
}