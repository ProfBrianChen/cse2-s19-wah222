import java.util.Scanner;
public class ArrayGames{
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter zero to shorten, one to insert: ");
        int choice = scan.nextInt();
        if( choice == 0){ //SHORTEN
            int[] arr1 = generate();
            System.out.println("Enter the index to shorten: ");
            int index = scan.nextInt();
            System.out.print("Input 1: ");
            print( arr1 );
            System.out.println("Input 2: " + index);
            int[] out = shorten( arr1, index);
            System.out.print("Output: ");
            print( out );
            
        }
        else if( choice == 1){
            int[] arr1 = generate();
            int[] arr2 = generate();
            System.out.print("Input 1: ");
            print( arr1 );
            System.out.print("Input 2: ");
            print( arr2 );
            int[] arr3 = insert( arr1, arr2 );
            System.out.print("Output: ");
            print( arr3 );

        }
        else{
            System.out.println("Invalid input.");
        }
    }
    public static int[] generate(){
        int random = (int)(Math.random() *10) + 10;
        int[] boy = new int[random];
        for( int i = 0; i < random; i++){
            int in = (int)(Math.random() *20);
            boy[i] = in;
        }
        return boy;
    }
    public static void print( int[] arr ){
        System.out.print("{");
        for(int i = 0; i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println("}");
    }
    public static int[] insert( int[] arr1, int[] arr2 ){
        int totLength = arr1.length + arr2.length;
        int girl[] = new int[totLength];
        int num = (int)(Math.random() * arr1.length);
        for(int i = 0; i <= num; i++){
            girl[i] = arr1[i];
        }
        for(int j = 0; j < arr2.length; j++){
            girl[j+num+1] = arr2[j];
        }
        for(int k = num + 1; k < arr1.length; k++){
            girl[ arr2.length + k] = arr1[k];
        }
        return girl;
    }
    public static int[] shorten( int[] arr1, int num ){
        int[] arr2 = new int[arr1.length -1];
        if( num < arr1.length -1){
            for(int i = 0; i < num; i++){
                arr2[i] = arr1[i];
            }
            for( int i = num; i < arr1.length -1; i++){
                arr2[i] = arr1[i +1];
            }
            return arr2;
        }
        else{
            return arr1;
        }
    }
}