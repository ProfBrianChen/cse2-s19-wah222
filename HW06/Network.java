import java.util.Scanner; //SCANNER WILL BE NECCESARY
public class Network{
    public static void main(String args[]){

        int height = collect("Height"); //Collects dimensions suing method that guarantees a valid input
        int width = collect("Width");
        int side = collect("Side");
        int length = collect("length");


        int middle = 0;
        if( side % 2 == 0){
            middle = side / 2; //Another as well
        }
        else if( side % 2 == 1){
            middle = Math.round(side / 2);
        }
        boolean mid = side % 2 == 0;

        int boxNumVert = 0;  //Vertical box number should not be affected by changing rows
        for( int row = 1; row <= height; row++){  //for loop that goes through rows one by one
            int boxNumHoz = 0;  //Horizontal box number should reset to zero upon new row

            for( int column = 1; column <= width; column++){  //for loop that navigates through every spot of each row(columns)

                if((column == (boxNumHoz * length + boxNumHoz * side + length + side + 1))){ //After every one side and one length horizontally, one box number is added
                    boxNumHoz++;
                }
                else if((row == boxNumVert * side + boxNumVert * length + length + side + 1)){ //Same but vertical
                    boxNumVert++;
                }

                int Hoz = boxNumHoz; //Redifined for simplicity
                int Vert = boxNumVert;

            // BEGINNING OF CONDITIONS CHECKED AT EVEERY COLUMN OF THE CURRENT ROW
                //EDGES OF BOXES
                if( ((row == Vert * side + Vert * length + 1) || (row == Vert * side + Vert * length + side)) && ((column == (Hoz * length + Hoz * side + side)) || (column == (Hoz * length + Hoz * side + 1)))){
                    System.out.print("#");//EVENT WHERE THE CORNERS OF EACH BOX OCCURS(DETERMINED BY THE COORDINATES)
                }


                //HORIZONTAL AND VERTICAL BOX LINES
                else if( ((row == Vert * length + Vert * side + 1 || row == Vert * length + Vert * side + side)) && ((column < Hoz * side + Hoz * length + side) && (column > Hoz * length + Hoz * side + 1))){
                    System.out.print("-"); //LINES IN BETWEEN BOXES, OCCUR AT CONSTANT ROWS WITH PATTERN AND RANGE OF COLUMNS
                }
                else if( (row > Vert * side + Vert * length + 1 && row < Vert * side + Vert * length + side) && (column == Hoz * side + Hoz * length + side || column == Hoz * length + Hoz * side + 1)){
                    System.out.print("|"); // SAME BUT THE VERTICAL VERSION: CONSTANT COLUMNS BUT RANGE OF ROWS
                }


                //HORIZONTAL AND VERTICAL LENGTHS IF ODD
                else if( (mid == false) && (row == middle + Vert * length + Vert * side + 1) && (column < Hoz * length + Hoz * side + length + side + 1 && column > Hoz * length + Hoz * side + side)){
                    System.out.print("-"); //SAME IDEA: ROW CONDITION MUST BE MET AND COLUMN MUST FALL WITHING THE RANGE(UPPER AND LOWER BOUND)
                } //Good
                else if( (mid == false) && ((row > side * Vert + length * Vert + side) && (row < length * Vert + side * Vert + side + length + 1)) && ( column == middle + Hoz * length + Hoz * side + 1)){
                    System.out.print("|"); //THE VERTICAL VERSION(THIS SECTION MAKES ONE SINGLE LINE(FOR ODD SIDES))
                }


                //HORIZONTAL AND VERTICAL LENGTHS IF EVEN
                else if( mid && ( (row == middle + Vert * side + Vert * length) || (row == middle + Vert * side + Vert * length + 1) ) && (column > Hoz * side + Hoz * length + side && column < Hoz * side + Hoz * length + side + length + 1)){
                    System.out.print("-"); //SAME THING BUT FOR TWO ROWS(FOR EVEN SIDES)
                } //Good
                else if( mid && (((row > side * Vert + length * Vert + side) && (row < length * Vert + side * Vert + side + length + 1)) && (( column == middle + Hoz * length + Hoz * side) || ( column == middle + Hoz * length + Hoz * side + 1)))){
                    System.out.print("|"); //VERTICAL VERSION
                } //Good

                else{
                    System.out.print(" "); //IF NO CONDITIONS ARE MET, MUST BE A BLANK SPOT
                }
            }
            System.out.println(); //BEGINS THE NEXT LINE(AFTER THE FINAL CONDITION OF THE ROW LOOP IS MET)
        }

    }


    public static int collect( String want ){
        int goodBoy = 0;
        Scanner ear = new Scanner(System.in);
        while( true ){
            System.out.println("Enter a positive integer value for " + want + ": ");  //promts
            if( ear.hasNextInt() ){   //Checks if there is an int
        	    int value = ear.nextInt();  //Assigns this int into a preliminary value length
                if( value > 0 ){ //Check if it is positive
                    goodBoy = value; //Assigns value to integer
                    break; //Leaves loop
                }
                else{
                    System.out.println("That is not a positive integer.");  //Case where negative int is entered
                }
            }
            else{
            System.out.println("Wrong type."); //Case where letter is entered
            ear.next(); //Trash value
            }
        }
        return goodBoy;
    }
}
