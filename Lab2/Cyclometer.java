public class Cyclometer {
  //Main method always required
 public static void main(String args[]){
int secsTrip1 = 480;   	//Seconds for each trip
int secsTrip2 = 3220;
int countsTrip1 = 1561;	//Number of rotations per trip
int countsTrip2 = 9037;
    
    
double wheelDiameter=27.0;  //Useful fact on wheel diamter
double PI=3.14159;   //Useful fact of pi
int feetPerMile=5280;   //Useful fact of unit conversion
int inchesPerFoot=12;  	//Another unit conversion
float secondsPerMinute=60; 	//Yet another one
double distanceTrip1, distanceTrip2,totalDistance;   	//Declaring types of variables before hand
    
    
System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
	//Printing the info of each trip
    
    
distanceTrip1 = countsTrip1 * wheelDiameter * PI;   //Gives distance in inches
distanceTrip1/= inchesPerFoot * feetPerMile; //Gives distance in miles
distanceTrip2 = countsTrip2 * wheelDiameter * PI/inchesPerFoot/feetPerMile;   //Gives distance in
totalDistance = distanceTrip1 + distanceTrip2;  //Total distance of trip

System.out.println("Trip 1 was "+ distanceTrip1 + " miles"); 	//Printing out the resulting distance of trip 1 in miles
System.out.println("Trip 2 was "+distanceTrip2 + " miles"); 	//Printing out trip 2 in miles
System.out.println("The total distance was "+totalDistance+" miles");   //Printing out the total in miles
    
    
  }
}
