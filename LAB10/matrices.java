public class matrices{
    public static void main(String args[]){
        int randomh = (int)(3 * Math.random() +2);
        int randomw = (int)(3 * Math.random() +2);
        int randomh2 = (int)(3 * Math.random() +2);
        int randomw2 = (int)(3 * Math.random() +2);
        int[][] A = increasingMatrix( randomw, randomh, true);
        int[][] B = increasingMatrix( randomw, randomh, false);
        int[][] C = increasingMatrix( randomw2, randomh2, true);

        System.out.println("A: \n");
        printArray( A, true);
        System.out.println("B: \n");
        printArray( B, false);
        System.out.println("C: \n");
        printArray( C, true);

        int[][] sumAB = addMatrix(A, true, B, false);
        int[][] sumAC = addMatrix(A, true, C, true);
        System.out.println("The sum of A and B: \n");
        printArray( sumAB, true);
        System.out.println("The sum of A and C: \n");
        printArray( sumAC, true);
    }
    public static int[][] increasingMatrix( int width, int height, boolean format){
        int count = 1;
        int[][] arr;
        if( format){
            arr = new int[height][width];
            for(int p = 0; p < height; p++){
                for(int i = 0; i < width; i++){
                    arr[p][i] = count;
                    count++;
                }
            }
        }
        else{
            arr = new int[width][height];
            for(int p = 0; p < height; p++){
                for(int i = 0; i < width; i++){
                    arr[i][p] = count;
                    count++;
                }
            }
        }
        return arr;
    }
    public static void printArray( int[][] arr, boolean format){
        if(arr == null){
            System.out.println("The array was empty!");
            return;
        }
        if( format){
            for(int i = 0; i < arr.length; i++){
                for(int g = 0; g < arr[0].length; g++){
                    System.out.print(arr[i][g] + "\t");
                }
                System.out.println();
            }
        }
        else{
            arr = translate( arr);
            for(int i = 0; i < arr.length; i++){
                for(int g = 0; g < arr[0].length; g++){
                    System.out.print(arr[i][g] + "\t");
                }
                System.out.println();
            }
        }
    }
    public static int[][] translate( int[][] arr){
        int[][] new1 = new int[arr[0].length][arr.length];
        for( int i = 0; i < arr[0].length; i++){
            for(int f = 0; f < arr.length; f++){
                new1[i][f] = arr[f][i];
            }
        }
        return new1;
    }
    public static int[][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb){
        if( formata == false){
            a = translate(a);
        }
        else if( formatb == false){
            b = translate(b);
        }
        if(a.length != b.length || a[0].length != b[0].length){
            System.out.println("Arrays cannot be added!");
            return null;
        }
        int[][] result = new int[a.length][a[0].length];
        for( int i = 0; i < a.length; i++){
            for(int w = 0; w < a[0].length; w++){
                result[i][w] = a[i][w] + b[i][w];
            }
        }
        return result;
    }

}