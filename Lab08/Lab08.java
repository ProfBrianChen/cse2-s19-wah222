import java.util.Arrays;
public class Lab08{
  public static void main(String[] args) {
    int num = (int)(Math.random()*50) + 49;
    int[] myArray = new int[num];
    for(int i = 0; i <= num-1; i++){
      myArray[i] = (int)(Math.random() * 99);
    }
    System.out.println("The random values are: ");
    System.out.println();
    for(int i = 0; i <= num-1; i++){
      System.out.print(myArray[i] + " ");
    }
    int[] myArray1 = myArray;
    int[] myArray2 = myArray;
    int[] myArray3 = myArray;

    int range = getRange( myArray1 );
    System.out.println("\nThe range is " + range);

    double mean = getMean( myArray2 );
    System.out.println("The mean is " + mean);
    double std = getStd( myArray3 , mean);
    System.out.println("The Standard deviation is " + std);
    shuffle( myArray);
    System.out.println();
  }
  public static int getRange( int[] myArray){
    Arrays.sort( myArray);
    int range = myArray[myArray.length-1] - myArray[0];
    return range;
  }
  public static double getMean( int[] myArray){
    int sum = 0;
    for(int i = 0; i <= myArray.length-1; i++){
      sum += myArray[i];
    }
    double mean = sum / myArray.length;
    return mean;
  }
  public static double getStd( int[] myArray, double mean){
    double sumDiff = 0;
    for(int i = 0; i <= myArray.length-1; i++){
      sumDiff += (Math.pow((myArray[i] - mean), 2));
    }
    double std = Math.sqrt(sumDiff /(myArray.length));
    return std;
  }
  public static void shuffle( int[] myArray){
    for(int i = 0; i <= myArray.length-1; i++){
      int random = (int)(Math.random() * 50);
      int save = myArray[random];
      myArray[random] = myArray[i];
      myArray[i] = save;
    }
    System.out.println("The shuffled values are: ");
    for(int i = 0; i <= myArray.length-1; i++){
      System.out.print(myArray[i] + " ");
    }
    }
  }
