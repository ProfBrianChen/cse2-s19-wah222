import java.util.Scanner;
public class Triangle4{
    public static void main(String args[]){
        Scanner ear = new Scanner(System.in);

        int request = 2;
        //GATHER INPUT
        while( true ){
            System.out.println("Enter a positive integer between 1 and 10: ");  //promts
            if( ear.hasNextInt() ){   //Checks if there is an int
                int length = ear.nextInt();  //Assigns this int into a preliminary value length
              if( length > 0 ){ //Check if it is positive
                if(length > 1 && length < 10){
                    request = length; //Assigns value to integer
                    break; //Leaves loop
                }
                else{
                    System.out.println("That integer is not between 1 and 10.");
                }
              }
              else{
                System.out.println("That is not a positive integer.");  //Case where negative int is entered
              }
            }
            else{
              System.out.println("Wrong type."); //Case where letter is entered
              ear.next(); //Trash value
            }
          }

        for(int row = 1; row <= request; row++){ //LOOP FOR ROWS
            for(int column = 1;column <= request; column++){ //LOOP FOR COLUMNS
                if(column <= request - row +1){ //CHECKS TO SEE IF CURRENT COLUMN FALLS WITHIN BOUNDS
                    System.out.print(request - column +1 - row +1); //PRINTS THE COLUMN IF CONDITION IS MET. +1's are to compensate for using row and column index 1 above what java would prefer
                }
                else{
                    System.out.print(" "); //PRINTS A SPACE IF NO CONDITION IS MET
                }
            }
            System.out.println(); //STARTS THE NEXT ROW
        }
    }
}