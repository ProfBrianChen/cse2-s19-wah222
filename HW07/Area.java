import java.util.Scanner;
public class Area{


    public static double area(){ //The 'main' method
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter either rectangle, triangle or circle to find the area: ");
        double area = 0;
        while( true ){
            if(scan.hasNextDouble()){ //CASE WHERE NUMBER IS ENTERED
                System.out.print("No numbers, enter rectangle, triangle or circle: ");
                double trash = scan.nextDouble();
            }
            else{ //IF NOT NUMBER, MUST BE STRING
                String input = scan.next();
                if(input.equals("triangle")){ //CASE OF TRIANGLE
                    area = triangle(); //CALLS SPECIFIC SUB-METHOD
                    break; //BREAKS OUT OF LOOP
                }
                else if(input.equals("rectangle")){ //CASE OF RECTANGLE
                    area = rectangle();
                    break;
                }
                else if(input.equals("circle")){ //CASE OF CIRCLE
                    area = circle();
                    break;
                }
                else{ //CASE OF INVALID STRING
                    System.out.println("Enter a valid string input--rectangle, triangle or circle: ");
                }
            }
        }
    return area; //RETURNS AREA TO MAIN METHOD
    }






    public static double triangle(){ //AREA FOR TRIANGLES
        double height = doubles("height"); //EACH DIMENSION CALCULATED CALLS FOR METHOD THAT ENSURES A POSITIVE DOUBLE
        double length = doubles("length");
        double area = 0.5 * height * length;
        return area; //RETURNS AREA TO ABOVE METHOD
    }

    public static double rectangle(){ //AREA FOR RECTANGLES
        double height = doubles("height");
        double length = doubles("length");
        double area = height * length;
        return area;
    }

    public static double circle(){ //AREA FOR CIRCLES
        double radius = doubles("radius");
        double area = Math.PI * radius * radius;
        return area;
    }






    public static double doubles( String want){ //METHOD FOR EACH DIMENSION TO ENSURE THE VALUE IS DOUBLE AND POSITIVE
        double doub = 0;
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the " + want + ": ");
        while( true ){
            if( scan.hasNextDouble() ){ //CHECKS TO MAKE SURE INPUT IS DOUBLE
                double input = scan.nextDouble();
                if( input > 0){ //CHECKS TO MAKE SURE ENTERED DOUBLE IS POSITIVE
                    doub = input;
                    break; //LEAVES LOOP
                }
                else{
                    System.out.println("Invalid, enter a positive number for the " + want +": ");
                }
            }
            else{ //NP STRINGS ALLOWED
                System.out.println("Invalid, enter a positive number for the " + want + ": ");
                double trash = scan.nextDouble(); //TRASHES WRONG INPUT
            }
        }
        return doub;
    }





    public static void main(String args[]){

        double area = area(); //CALLS FOR THE BIG METHOD
        System.out.println("The area is " + area + " units squared."); //PRINTS OUTCOME
    }
}