import java.util.Scanner;
public class StringAnalysis{

    public static boolean stringCheck( String input ){ //METHOD THAT, WHEN CALLED, CHECKS EVERY CHARACTER TO SEE IF IT IS A LETTER
        boolean perfect = false;
        int counter = 0;
        int length = input.length();

        for( int i = 0; i < length; i++ ){ //LOOPS THROUGH ALL INDEXES
            if( Character.isLetter(input.charAt(i)) ){
                counter++;
            }
        }

        if( counter == input.length() ){ //CHECKS IF ALL INDEXES WHERE LETTERS
            perfect = true;
        }
        return perfect;
    }

    public static boolean stringCheck( String input, int upTo ){ //SAME TYPE OF METHOD BUT FOR WHEN A RESTRICTION TO HOW MANY CHARACTERS TO CHECK IS WANTED
        boolean perfect = false;
        int counter = 0;

        for( int i = 0; i < upTo; i++ ){ //LOOPS ALL THE INDEXES UP TO THE USER'S REQUEST
            if( Character.isLetter(input.charAt(i)) ){
                counter++;
            }
        }

        if( counter == upTo ){ //CHECKS IF ALL WERE LETTERS
            perfect = true;
        }
        return perfect;
    }




    public static String giveString(){ //METHOD THAT ENSURES A STRING IS INPUTED
        Scanner scan = new Scanner(System.in);
        String word = "";
        System.out.println("Enter the string: ");

        while( true ){
            if( scan.hasNextDouble()){
                System.out.println("No numbers, enter a string: ");
                String trash = scan.next();
            }
            else{
                word = scan.next();
                break;
            }
        }
        return word;
    }

    public static int giveInt(){ //METHOD THAT ENSURES A POSITIVE INT IS ENTERED FOR HOW MANY CHARACTERS TO CHECK
        Scanner scan = new Scanner(System.in);
        int length = 0;
        System.out.println("Enter how many characters to check: ");
        while( true ){
            if( scan.hasNextInt() ){ //CHECKS THAT IT IS AN INT
                int input = scan.nextInt();
                if( input > 0){ //CHECKS THAT IT IS POSITIVE
                    length = input;
                    break;
                }
                else{
                    System.out.println("Enter a positive number for characters to check: "); //CASE WHERE NEGATIVE INT IS ENTERED
                }
            }
            else{
                System.out.println("Enter an integer for number of characters to check: "); //CASE WHERE STRING IS ENTERED
                String trash = scan.next();
            }
        }
        return length;
    }









    public static boolean getBool(){ //BIG METHOD. RETURNS WHETHER OR NOT ALL CHARACTERS ARE LETTERS

        boolean perfect = false;
        Scanner scan = new Scanner(System.in);
        String decision = "";

        String word = giveString(); //REQUESTS THE WORD

        System.out.println("Do you wish to check all characters or a certain amount? Enter 'all' or 'restrict': ");
        while( true ){ //LOOP THAT ASKS USER IF THEY WOULD LIKE TO RESTRICT HOW MANY CHARATERS TO CHECK
            if( scan.hasNextDouble()){ //NO NUMBERS ALLOWED
                System.out.println("No numbers, enter 'all' or 'restrict' for whether you wish to check all characters or not: ");
                double trash = scan.nextDouble(); //TRASH VALUE
            }
            else{ //IF NOT NUMBER, MUST BE STRING
                decision = scan.next();
                if( decision.equals("all")){
                    perfect = stringCheck(word); //IF REQUEST IS ALL, ONLY THE WORD WILL NEED TO BE ENTERED TO METHOD
                    break;
                }
                else if(decision.equals("restrict")){
                    int length = giveInt(); //IF REQUEST IS RESTRICT, A POSITIVE INT FOR LENGTH WILL NEED TO BE COLLECTED
                    perfect = stringCheck(word, length); //THEN THE TWO VALUES ENTERED INTO METHOD
                    break;
                }
                else{
                    System.out.println("Enter either 'all' or 'restrict' for whether you wish to check all characters or not: "); //CASE OF WRONG STRING
                }
            }
        }
        return perfect;
    }

    public static void main(String args[]){

        boolean perfect = getBool(); //CALLS METHOD TO CHECK THE CHARACTERS

        if( perfect == true){ //PRINTS THE APPROPRIATE RESPONSE ACCORDING TO METHOD RESULT
            System.out.println("Every character is a letter!");
        }
        else{
            System.out.println("Not every character is a letter.");
        }

    }
}