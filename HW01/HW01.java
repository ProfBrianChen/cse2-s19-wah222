public class HW01{
  public static void main(String args[]){
    System.out.println("    -----------");     //Printing line-by-line
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^  ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-W--A--H--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
    System.out.println("  v  v  v  v  v  v  ");
    System.out.println("My name is Wilmer Hernandez");
    System.out.println("and I am from Atlanta Georgia");
    System.out.println("but I was born in Honduras");
  }
}