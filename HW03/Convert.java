import java.util.*;   //SUMMONS SCANNERS
public class Convert{
  public static void main(String args[]){
    Scanner ear = new Scanner(System.in); //SUMMONS SCANNER NAMED EAR
    System.out.println("How many meters?: "); //PROMPTS FOR METERS
    double meters = ear.nextDouble();  //COLLECTS DATA
    double inches = meters * 39.3701;   //CONVERTS TO INCHES
    System.out.println(meters + " meters is the same as " + inches + " inches!");   //PRINTS IN INCHES
  }
}