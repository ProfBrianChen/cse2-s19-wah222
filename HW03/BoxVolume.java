import java.util.*;    //SUMMONS SCANNERS
public class BoxVolume{
  public static void main(String args[]){
    Scanner ear = new Scanner(System.in);   //ADDS SCANNER NAMED EAR
    System.out.println("Enter the length: ");  //PROMPTS FOR LENGTH
    double Length = ear.nextDouble();       //COLLECTS LENGTH
    System.out.println("Enter the width: ");   //PROMPTS FOR WIDTH
    double Width = ear.nextDouble();        //COLLECTS WIDTH
    System.out.println("Enter the height: ");  //PROMPTS FOR HEIGHT
    double Height = ear.nextDouble();       //COLLECTS HEIGHT
    
    double volume = Length * Width * Height;   //CALCULATES VOLUME
    
    System.out.println("The volume of the box is " + volume + " units cubed!");   //PRINTS OUT VOLUME
  }
}