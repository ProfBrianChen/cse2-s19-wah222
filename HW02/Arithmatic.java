public class Arithmatic {
    public static void main(String args[]) {
        int numPants = 3;
        double pantsPrice = 34.98;       //Given fact

        int numShirts = 2;
        double shirtPrice = 24.99;       //Given fact

        int numBelts = 1;
        double beltPrice = 33.99;        //Given fact

        double taxRate = 0.06;
        double afterTax = 1.06;          //Given fact

        double TPPants = numPants * pantsPrice;       //TP stands for Total Price
        double TPShirts = numShirts * shirtPrice;     //This gives total price before tax for each
        double TPBelts = numBelts * beltPrice;

        System.out.println("\nThe total price before tax is $" + TPPants + " for pants, $" + TPShirts
                + " for shirts and $" + TPBelts + " for belts.");    //Prints the totals for each component

        int taxPantsi = (int) ((TPPants * afterTax - TPPants) * 100);    //Subtracts total from total with tax to find tax for each component
        int taxShirtsi = (int) ((TPShirts * afterTax - TPShirts) * 100);  //variables named with i at the end to prepare for process to trunk to two decimals
        int taxBeltsi = (int) ((TPBelts * afterTax - TPBelts) * 100);    //Multiplied times 100 to put two decimals to the left of point, converted to int to trunk decimals

        double taxPants = (double) taxPantsi / 100;   //Converted to double then divided by 100 to put two decimals back
        double taxShirts = (double) taxShirtsi / 100; //Variables representing tax totals for each component to two decimals
        double taxBelts = (double) taxBeltsi / 100;

        System.out.println("\nThe total tax that will be charged is $" + taxPants + " for pants, $" + taxShirts
                + " for shirts and $" + taxBelts + " for belts.");  //Prints out taxes for each component

        double TnoTax = TPPants + TPShirts + TPBelts;  //Finds total before tax by adding component
        double TTax = taxPants + taxShirts + taxBelts; //Finds total tax by adding tax components

        System.out.println("\nThe total before tax is $" + TnoTax + " and the total tax charged is $" + TTax); //Prints out the previous

        double TotalCost = TnoTax + TTax; //Finds total by adding total before tax and total tax
        System.out.println("\nThe total cost of the transaction $" + TotalCost);  //Prints total
    }
}