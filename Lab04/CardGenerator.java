import java.lang.Math;
public class CardGenerator{
  public static void main(String args[]){
    int numc = (int)(Math.random() * 52);  //Generates the random card 1-52
    int numd = numc % 13;
    
    String cnum = "";  //Prevents bug
    
    switch( numd ){
        case 0:
             cnum = cnum + "King"; //All of the cases of card number
             break;
        case 1:
             cnum = cnum + "Ace";
             break;
        case 2:
             cnum = cnum + numd;
             break;
        case 3:
             cnum = cnum + numd;
             break;
        case 4:
             cnum = cnum + numd;
             break;
        case 5:
             cnum = cnum + numd;
             break;
        case 6:
             cnum = cnum + numd;
             break;
        case 7:
             cnum = cnum + numd;
             break;
        case 8:
             cnum = cnum + numd;
             break;
        case 9:
             cnum = cnum + numd;
             break;
        case 10:
             cnum = cnum + numd;
             break;
        case 11:
             cnum = cnum + "Jack";
             break;
        case 12:
             cnum = cnum + "Queen";
             break;
    }
    String deck = "";  //Prevents bug
    
    if(0 < numc && numc <= 13){
      deck = deck + "Diamonds";       //All of the cases of deck name
    }
    else if(14 <= numc && numc <=26){
      deck = deck + "Clubs";
    }
    else if(27 <= numc && numc <=39){
      deck = deck + "Hearts";
    }
    else if(40 <= numc && numc <=52){
      deck = deck + "Spades";
    }
    
    System.out.println("You picked the " + cnum + " of " + deck);   //Prints result
    }
}