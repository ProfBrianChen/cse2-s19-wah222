import java.util.Scanner; //Importing scanner object
public class TwistGenerator{
  public static void main(String args[]){
	  
    Scanner ear = new Scanner(System.in); //Declaring scanner named ear
    int integer = 0;  //Declare length requested as zero for loops to change
    
    while( true ){
        System.out.println("Enter a positive integer: ");  //promts
        if( ear.hasNextInt() ){   //Checks if there is an int
        	int length = ear.nextInt();  //Assigns this int into a preliminary value length
          if( length > 0 ){ //Check if it is positive
            integer = length; //Assigns value to integer
            break; //Leaves loop
          }
          else{
            System.out.println("That is not a positive integer.");  //Case where negative int is entered
          }
        }
        else{
          System.out.println("Wrong type."); //Case where letter is entered
          ear.next(); //Trash value
        }
      }
   
    int integer1 = integer;  //Splits integer into three, one for each line that is to be printed
    int integer2 = integer;
    int integer3 = integer;
    
    
    while( integer1 > 0 ){  //First line of print, will stop when int credits are exhausted
      if( integer1 > 0){ //Checks that there are still values remaining after each loop
        System.out.print("/"); //If there is int left, adds this character
        integer1--; //Charges one int for adding the character
      }
      if( integer1 > 0 ){
        System.out.print("\\"); //Same but for this character
        integer1--;
      }
      if( integer1 > 0){
        System.out.print(" "); //Same
        integer1--;
      }
  }
    System.out.println(""); //Begins the second line
    
    
    while( integer2 > 0 ){
      if( integer2 > 0){
        System.out.print(" ");  //Same concept but different pattern of characters
        integer2--;
      }
      if( integer2 > 0 ){
        System.out.print(" ");
        integer2--;
      }
      if( integer2 > 0){
        System.out.print("X");
        integer2--;
      }
  }
    System.out.println("");  //Begins the third line
    
     while( integer3 > 0 ){
      if( integer3 > 0){
        System.out.print("\\");  //Same concept but different pattern of characters
        integer3--;
      }
      if( integer3 > 0 ){
        System.out.print("/");
        integer3--;
      }
      if( integer3 > 0){
        System.out.print(" ");
        integer3--;
      }
  }
    System.out.println(""); //Not neccesary, but makes the print cleaner
    
}
}
                         