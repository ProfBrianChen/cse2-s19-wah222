public class RobotCity{
    public static void main(String args[]){
        int k = (int)(Math.random()*9) +1; //+1 TO SHOW AT LEAST ONE BOT
        int[][] arr = buildCity();
        display( arr );
        arr = invade( arr, k);
        display( arr);
        System.out.println("5 UPDATES:");
        for(int i = 0; i < 5; i++){ //FIVE ITERATIONS OF UPDATE
            arr = update( arr);
            display( arr );
        }
    }
    public static int[][] buildCity(){ //Straight forward build a population box
        int up = (int)(Math.random()*5) + 10;
        int right = (int)(Math.random()*5) + 10;
        int[][] arr = new int[right][up];

        for(int i = 0; i < right; i++){
            for(int j = 0; j < up; j++){
                int pop = (int)(Math.random()*899) +100;
                arr[i][j] = pop;
            }
        }
        return arr;
    }
    public static void display( int[][] arr){ //BASIC PRINTING
        for(int i = 0; i < arr[0].length; i++){
            for(int j = 0; j < arr.length; j++){
                System.out.printf(arr[j][i] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
    public static int[][] invade( int[][] arr, int k){
        int[] columns = new int[k -1]; //EXTRA ARRAYS TO KEEP UP WITH DUPLICATES
        int[] rows = new int[k -1];

        for(int g = 0; g < columns.length; g++){ //ASSEMBLES ARRAYS TO KEEP UP WITH DUPLICATES
            columns[g] = -1;
            rows[g] = -1;
        }

        for(int i = 0; i < k; i++){
            int index;
            boolean dup = false;
            int row = (int)(Math.random() *(arr[0].length));
            int column = (int)(Math.random() *(arr.length));

            for(int j = 0; j < columns.length; j++){ //CHECKS WITH EXTRA ARRAYS IF IT IS A DUPLICATE POSITION
                if( columns[j] == column){
                    if(rows[j] == row){
                        dup = true;
                    }
                }
            }
            if(dup){ //IF IT IS SKIP
                i--;
                continue;
            }
            arr[column][row] = (-1)* arr[column][row]; //IF NOT, MAKE NEGATIVE
        }
        return arr;
    }
    public static int[][] update( int[][] arr ){
        for(int i = arr.length -1; i >= 0; i--){
            for(int j = 0; j < arr[0].length; j++){
                if( arr[i][j] < 0 && i < arr.length -1){ //IF NOT THE RIGHT CORNER, MAKE POSITIVE AND ONE TO THE RIGHT NEGATIVE
                    arr[i][j] = (-1) * arr[i][j];
                    arr[i +1][j] = -1 * arr[i +1][j];
                }
                else if( arr[i][j] < 0 && i == arr.length -1){ //IF THE RIGHTMOST, MAKE POSITIVE ONLY
                    arr[i][j] = (-1) * arr[i][j];
                }
            }
        }
        return arr;
    }
}