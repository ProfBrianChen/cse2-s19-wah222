public class straight1{
    public static void main(String args[]){
        int count = 0;
        for(int g = 0; g < 1000000; g++){
            int[] hand = firstFive(shuffledDeck());
            int level = getLevel(hand, 1);
            int goodness = 0;

            for(int p = 0; p < 5; p++){
                for(int i = 0; i < 5; i++){
                    if( hand[i] == level + p){
                        goodness++;
                    }
                }
            }
            int bad = 0;
            for(int k = 0; k < 5; k++){
                if( hand[k] == 12 || hand[k] == 25 || hand[k] == 38){
                    bad++;
                }
                else if( hand[k] == 13 || hand[k] == 26 || hand[k] == 39){
                    bad++;
                }
            }
            if( bad == 2){
                goodness--;
            }
            if( goodness == 5){
                System.out.println("Got one!");
                for(int i = 0; i < 5; i++){
                    System.out.print(hand[i] + ";");
                }
                System.out.println();
                count++;
            }
        }
        double total = (double) count;
        total /= 10000;
        System.out.println("The percentage of straights is: " + total + "%");
    }
    public static int[] shuffledDeck(){
        int[] deck = new int[52];
        for( int i = 0; i < 52; i++){
            deck[i] = i+1;
        }
        for( int j = 0; j < 52; j++){
            int index = (int)(Math.random() * 52);
            int mid2 = deck[index];
            deck[index] = deck[j];
            deck[j] = mid2;
        }
        return deck;
    }
    public static int[] firstFive( int[] arr){
        int[] five = new int[5];
        for(int i = 0; i < 5; i++){
            five[i] = arr[i];
        }
        return five;
    }
    public static int getLevel( int[] arr, int level){
        int[] mid = new int[arr.length];
        for(int h = 0; h < arr.length; h++){
            mid[h] = arr[h];
        }
        if( level > 5 || level < 1){
            return -1;
        }
        for(int i = 0; i < mid.length; ++i){
            int j = i;
            while( j > 0 && mid[j] < mid[j -1]){
                int middle = mid[j];
                int mid2 = mid[j -1];
                mid[j] = mid2;
                mid[j -1] = middle;
                j--;
            }
        }
        return mid[level -1];
    }
}